<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::group(['middleware' =>['loginputs']], function () {
    Route::get('/', 'HomeController@getHomeContent')->name('home');
    Route::get('/home', 'HomeController@getHomeContent')->name('home');
    Route::group(['prefix'=>'customer/'], function () {
        Route::get('add', 'HomeController@getCustomerAdd');
        Route::post('store', 'HomeController@postAddCustomer');
        Route::get('list', 'HomeController@getCustomer');
        Route::get('list-data', 'HomeController@getCustomerData');
    });
    Route::group(['prefix'=>'user/'], function () {
    });
});

Route::group(['prefix'=>'user/'], function () {
    Route::get('update-profile', 'UserController@getUserProfileView');
    Route::post('post-update-profile', 'UserController@postUpdateProfile');
});
