$(document).ready(function () {
    
    // save insurance
    if ($("#save_form_submit").length > 0) {
        $("#save_form_submit").trigger('reset');

        var save_form = $("#save_form_submit");
        save_form.validate({
            errorPlacement: function errorPlacement(error, element)
            {
                element.after(error);
            },
            rules: {
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                address_line_1: {
                    required: true
                },
                address_line_2: {
                    required: true
                },
                city: {
                    required: true
                },
                state: {
                    required: true
                },
                zipcode: {
                    required: true
                },
                dob: {
                    required: true
                },
                ssn: {
                    required: true
                },
                is_term_accepted: {
                    required: true
                }
            },
            messages: {
                first_name: {
                    required: "First Name is required."
                },
                last_name: {
                    required: "Last Name is required."
                },
                address_line_1: {
                    required: "Street address is required"
                },
                address_line_2: {
                    required: "Apartment is required"
                },
                city: {
                    required: "City is required"
                },
                state: {
                    required: "State is required"
                },
                zipcode: {
                    required: "Zipcode is required"
                },
                dob: {
                    required: "Date of birth is required"
                },
                ssn: {
                    required: "SSN is required"
                },
                is_term_accepted: {
                    required: "Please accept the terms and conditions"
                },
            },
            submitHandler: function (form, event) {
                save_form_submit();
            }
        });
    }
});


function save_form_submit()
{
    $('.alert-success').addClass('hide');
    $('.alert-danger').addClass('hide');
    $('.alert-success .alert-content').html('');
    $('.alert-danger .alert-content').html('');
    var formData = new FormData($('#save_form_submit')[0]);
    $.ajax({
        type: "POST",
        url: config.updateProfile,
        data: formData,
        async: true,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
            if (response.success == 1) {
                window.location.href = '/home';
//                $('.alert-success').removeClass('hide');
//                $('.alert-success .alert-content').html(response.message).show();
//                setTimeout(function(){
//                    window.location.href = '/home';
//                }, 3000);
            } else {
                $('.alert-danger').removeClass('hide');
                if (response.message == '') {
                    $.each(response.result, function (key, value) {
                        $('.alert-danger .alert-content').html(value);
                    });
                } else {
                    $('.alert-danger .alert-content').html(response.message);
                }
            }
            $(".alert").fadeTo(5000, 500).slideUp(500, function () {
                $(".alert").slideUp(500);
            });
        },
        error: function (request, status, error) {
            $('.alert-danger').removeClass('hide');
            //$('.alert-danger .alert-content').html('Something went wrong. Please contact Quoting System').show();
            
            var error = jQuery.parseJSON(request.responseText);
            for (var errorField in error.errors) {
                if (error.errors.hasOwnProperty(errorField)) {
                    error.errors[errorField].forEach(function (value) {
                        $('.alert-danger .alert-content').html(value);
                    });
                }
            }
            $(".alert").fadeTo(5000, 500).slideUp(500, function () {
                $(".alert").slideUp(500);
            });
        }
    });
    return false;
}
