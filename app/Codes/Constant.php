<?php

/*
 * Copyright 2016-2017 Appster Information Pvt Ltd
 * All rights reserved
 * File: Constant.php
 * Benefil Wellness
 * Author: Gaurav Kakran
 * CreatedOn: 28/11/2016
 */


namespace App\Codes;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Constant
{
    public static $NULL= null;
    public static $PHOTO_LIMIT = 1;
    public static $NUMERIC = 1;
    public static $ALPHA_NUMERIC = 2;
    public static $OTP_LENGTH = 4;
    public static $OTP_EXPIRY_IN_DAYS = 1;
    public static $SLEEP_TIME_IN_SEC = 1;
    public static $ACTIVATE_TYPE = ['Yes'=>1,'No'=>2];
    public static $ACTIVATE_TYPE_STRING = [1 => 'Yes', 2 => 'No'];
    public static $IS_PROFILE_COMPLETE = ['Yes' => 1, 'No' => 2];
    public static $BY_OWNER = ['Yes'=>1,'No'=>2];
    public static $BOOL = [1=>'Yes',2=>'No'];
    public static $BOOL_STRING = ['Yes'=>1,'No'=>2];
    public static $PAGINATION = ['User'=>10];
    public static $SIGNUP_TYPE = ['EMAIL' => 1,'FB' => 2];
    public static $IS_EMAIL_VERIFIED = ['Yes' => 1, 'No' => 2];
    public static $IS_PHONE_VERIFIED = ['Yes' => 1, 'No' => 2];
    public static $LOGIN_TYPE = ['email'=>1, 'facebook'=>2];
    public static $FORGOT_PASSWORD_TYPE = ['email'=>1, 'phoneNumber'=>2];
    public static $PASSWORD_RESET_LENGTH = 6;
    public static $PROFILE_IS_PRIVATE = ['Yes'=>1,'No'=>2];
    public static $SENT_NOTIFICATION = ['Yes'=>1,'No'=>0];
    public static $IS_INVITED = ['Yes'=>1,'No'=>2];
    public static $SORT_CREDITS = ['A_Z'=>1,'Z_A'=>2];
    public static $NOTIFY_USER = ['Yes'=>1,'No'=>2];
    public static $NOTIFICATION_TYPES = ['User'=>1];
    public static $GENDER = ['Male' => 'Male', 'Female' => 'Female'];
    public static $CLOTHING_SIZE = ['Small' => 'Small', 'Medium' => 'Medium', 'Large' => 'Large', 'Extra Large' => 'Extra Large'];
}
