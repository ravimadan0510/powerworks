<?php

namespace App\Implementers;

use App\Contracts\UserContract;
use App\Helper\Utility\FileRepositoryLocal;
use App\Models\User;
use App\Helper\Utility\UtilityHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Helper\Utility\FileRepositoryS3;

class UserContractImpl extends BaseImplementer implements UserContract
{
    
    public static $page_title = 'page_title';
    public static $page_description = 'page_description';
    protected $file;

    public function __construct()
    {
        if (env('STORAGE_TYPE') == 'S3') {
            $this->file = new FileRepositoryS3();
        } else {
            //$this->file = new FileRepositoryLocal();
        }
    }
    
    /**
        *
        * Provide the view of user to update
        *
        * @param Null
        *
        * @return table view
        *
    */
    public function getUserProfileView()
    {
        $user = User::find(Auth::user()->id);
        if (empty($user)) {
            return $this->renderFailure(trans('messages.error.exception'), Response::HTTP_OK);
        }
        return view('user.profile')
                ->with('user', $user)
                ->with('page_title', 'Profile')
                ->with('page_description', 'Update your profile information');
    }
    
    /**
     *
     * @function postUpdateProfile
     * @description this function update the profile of the user
     * @param $request contains the data to update
     * @return
     */
    public function postUpdateProfile($request)
    {
        try {
                $data = $request->all();
            if ($request->hasFile('image_url')) {
                $data['image_url'] = $this->file->upload($request->file('image_url'), null, Auth::user()->id);
            }
                User::updateProfile($data);
                Session::put('success-message', trans('messages.user_profile_updated'));
                $return = $this->renderSuccess(trans('messages.user_profile_updated'));
        } catch (Exception $e) {
            UtilityHelper::logException(__METHOD__, $e);
            $return = $this->renderFailure(trans('messages.error.exception'), Response::HTTP_OK);
        }
        return $return;
    }
}
