<?php

namespace App\Contracts;

interface UserContract
{

    public function getUserProfileView();
    public function postUpdateProfile($request);
}
