<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Routing\Router;
use Auth;

class LogInputs
{
    
    protected $router;
    public $attributes;
    function __construct(Router $router)
    {
        $this->router=$router;
    }
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if ($user->is_profile_complete == 2) {
                return redirect('user/update-profile');
        }
        return $next($request);
    }
}
