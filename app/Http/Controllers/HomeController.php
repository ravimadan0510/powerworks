<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\CustomerDetail;
use App\Models\CleaningSchedule;
use App\Http\Requests\Customer\AddCustomerRequest;
use App\Helper\Utility\UtilityHelper;
use App\Codes\Constant;
use Auth;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    
    /**
     * Get home page content
     * @return type
     */
    public function getHomeContent()
    {
        return view('home');
    }
    
    /**
     * Form to add customer
     * @return type
     */
    public function getCustomerAdd()
    {
        $genderList = Constant::$GENDER;
        $clothingSize = Constant::$CLOTHING_SIZE;
        
        return view('customer.add')
                ->with('genderList', $genderList)
                ->with('clothingSize', $clothingSize);
    }
    
    /**
     * Add customer to list
     * @param AddCustomerRequest $request
     * @return type
     */
    public function postAddCustomer(AddCustomerRequest $request)
    {
        try {
            $reqData = $request->all();
            $userId = Auth::user()->id;
            
            $customerModel = new CustomerDetail();
            $customerModel->user_id = $userId;
            $customerModel->name = $reqData['name'];
            $customerModel->gender = $reqData['gender'];
            $customerModel->dob = $reqData['dob'];
            $customerModel->clothing_size = $reqData['clothing_size'];
            $customerModel->save();
            
            if ($request->hasFile('front_image_url')) {
                $imageUrl = $request->file('front_image_url');
                
                $extension = $imageUrl->getClientOriginalExtension();
                $imagePathAndName = time().'.'.$extension;
                
                Storage::disk('public')->put($imagePathAndName, File::get($imageUrl));
                
                $customerModel->front_image_url = $imagePathAndName;
            }
            
            if ($request->hasFile('side_image_url_1')) {
                $imageUrl1 = $request->file('side_image_url_1');
                
                $extension = $imageUrl1->getClientOriginalExtension();
                $imagePathAndName1 = (time()+10).'.'.$extension;
                
                Storage::disk('public')->put($imagePathAndName1, File::get($imageUrl1));
                
                $customerModel->side_image_url_1 = $imagePathAndName1;
            }
            
            if ($request->hasFile('side_image_url_2')) {
                $imageUrl2 = $request->file('side_image_url_2');
                
                $extension = $imageUrl2->getClientOriginalExtension();
                $imagePathAndName2 = (time()+20).'.'.$extension;
                
                Storage::disk('public')->put($imagePathAndName2, File::get($imageUrl2));
                
                $customerModel->side_image_url_2 = $imagePathAndName2;
            }
            
            $customerModel->save();
            
            return redirect('/customer/list')->with('message', trans('messages.customer_add_success'));
        } catch (Exception $e) {
            UtilityHelper::logException(__METHOD__, $e);
        }
    }
    
    /**
     * Get customer list
     * @param Request $request
     * @return type
     */
    public function getCustomer(Request $request)
    {
        $genderList = Constant::$GENDER;
        $clothingSize = Constant::$CLOTHING_SIZE;
        
        return view('customer.list')
                    ->with('genderList', $genderList)
                    ->with('clothingSize', $clothingSize);
    }
    
    /**
     * Get customer list datatable ajax
     * @param Request $request
     * @return type
     */
    public function getCustomerData(Request $request)
    {
        try {
            $reqData = $request->all();
            
            $uploadUrl = url('storage').DIRECTORY_SEPARATOR;
            
            $results = CustomerDetail::select(
                'id',
                'name',
                'gender',
                'front_image_url',
                'side_image_url_1',
                'side_image_url_2',
                DB::raw("YEAR(CURDATE())-YEAR(dob) as age"),
                'clothing_size',
                'created_at'
            );
            
            if (!empty($reqData['name'])) {
                $name = $reqData['name'];
                $results->where('name', 'like', "$name%");
            }
            
            if (!empty($reqData['gender'])) {
                $gender = $reqData['gender'];
                $results->where('gender', 'like', "$gender%");
            }
            
            if (!empty($reqData['age'])) {
                $results->where(DB::raw("YEAR(CURDATE())-YEAR(dob)"), $reqData['age']);
            }
            
            if (!empty($reqData['clothing_size'])) {
                $results->where('clothing_size', $reqData['clothing_size']);
            }
            
            //$results = $results->orderby('created_at', 'desc');
            
            $return = \Yajra\Datatables\Datatables::of($results);
            
            return $return->make(true);
        } catch (\Exception $e) {
            UtilityHelper::logException(__METHOD__, $e);
        }
    }
}
