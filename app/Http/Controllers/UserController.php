<?php

namespace App\Http\Controllers;

use App\Contracts\UserContract;
use Illuminate\Http\Request;
use App\Http\Requests\User\UpdateUsersRequest;

class UserController extends BaseController
{

    protected $admin;

    public function __construct(UserContract $admin)
    {
        parent::__construct();
        $this->admin = $admin;
        $this->middleware('auth', ['except' => []]);
    }
    
    /**
        *
        * Change style status
        * @param NULL
        * @throws Exception If something happens during the process
        *
    */
    public function getUserProfileView()
    {
        return $this->admin->getUserProfileView();
    }
    
    /**
        *
        * Change style status
        * @param NULL
        * @throws Exception If something happens during the process
        *
    */
    public function postUpdateProfile(UpdateUsersRequest $request)
    {
        return $this->admin->postUpdateProfile($request);
    }
}
