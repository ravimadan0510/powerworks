<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Codes\Constant;
use Hash;
use DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'address_line_1', 'address_line_2', 'zipcode', 'city', 'state', 'dob', 'ssn', 'is_profile_complete', 'created_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /**
     * Hash password
     * @param $input
     */
    public function setPasswordAttribute($input)
    {
        if ($input) {
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
        }
    }
    
    public static function updateProfile($data)
    {
        if (empty($data['user_id'])) {
            $user = new User();
        } else {
            $user = User::find($data['user_id']);
        }
        if (!empty($data['password'])) {
            $user->password = Hash::make($data['password']);
        }
        
        if (!empty($data['image_url'])) {
            $user->image_url = $data['image_url'];
        }
        
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->address_line_1 = $data['address_line_1'];
        $user->address_line_2 = $data['address_line_2'];
        $user->zipcode = $data['zipcode'];
        $user->city = $data['city'];
        $user->state = $data['state'];
        $user->dob = $data['dob'];
        $user->ssn = $data['ssn'];
        $user->is_profile_complete = Constant::$IS_PROFILE_COMPLETE['Yes'];
        $user->is_term_accepted = $data['is_term_accepted'];
        
        $user->save();
        return $user;
    }
    
    public static function createUser($reqData)
    {
        $reqData['first_name'] = $reqData['firstName'];
        $reqData['last_name'] = $reqData['lastName'];
        $user = User::create($reqData);
        
        return $user;
    }
}
