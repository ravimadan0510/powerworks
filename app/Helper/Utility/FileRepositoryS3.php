<?php
namespace App\Helper\Utility;

use App\library\aws\S3;
use Config;
use Storage;
use Log;

class FileRepositoryS3
{
    
    public function __construct()
    {
        $this->s3 = new S3(env('AWS_ACCESS_KEY_ID'), env('AWS_SECRET_ACCESS_KEY'));
    }
    
    public function upload($updloadObject, $type, $id = '', $name = null, $thumb = 0)
    {
        $returnFileName = "";
        if (is_object($updloadObject) && $updloadObject->isValid()) {
            $extension = $updloadObject->getClientOriginalExtension();
            if (empty($name)) {
                $filename = rand(1, 9999).time().'.'.$extension;
            } else {
                $filename =$this->getFilename($name);
            }
            $path=$this->getPath($type, $id);
            
            if ($id) {
                $uploadFileBucketPath = $path.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR.$filename;
            } else {
                $uploadFileBucketPath = $path.DIRECTORY_SEPARATOR.$filename;
            }
            
            $uploadResponse = Storage::disk('s3')->put($uploadFileBucketPath, file_get_contents($updloadObject->getRealPath()), 'public');
            if ($uploadResponse) {
                if ($thumb==1) {
                }
                $returnFileName = DIRECTORY_SEPARATOR.$uploadFileBucketPath;
            }
        }
        return $returnFileName;
    }
    
    public function uploadFileToS3($sourceFile, $type, $id)
    {
        try {
            $extension = pathinfo($sourceFile, PATHINFO_EXTENSION);
            $filename = rand(1, 9999).time().'.'.$extension;
            $path=$this->getPath($type, $id);
            $file = $path.DIRECTORY_SEPARATOR.$id.DIRECTORY_SEPARATOR.$filename;
            Storage::disk('s3')->put($file, file_get_contents($sourceFile), 'public');
            return DIRECTORY_SEPARATOR.$file;
        } catch (\Exception $e) {
            Log::error("uploadImageToS3() exception " . $e->getMessage());
            return false;
        }
    }

    public function deleteFileFromS3($pathfileName)
    {
        try {
            Storage::disk('s3')->delete($pathfileName);
            return $fileName;
        } catch (\Exception $e) {
            Log::error("deleteImageFromS3() exception " . $e->getMessage());
            return false;
        }
    }

    public static function deleteImageFromS3($fileName)
    {
        try {
            Storage::disk('s3')->delete($fileName);
            return $fileName;
        } catch (\Exception $e) {
            Log::error("deleteImageFromS3() exception " . $e->getMessage());
            return false;
        }
    }

    public function downloadImageFromFB($url, $user_id, $folder = 'temp')
    {
        $img = public_path().'/uploads/'.$folder.'/'.$user_id.'.jpg';
        file_put_contents($img, file_get_contents($url));
        return $img;
    }
    
    private function getPath($type, $id = "")
    {
        switch ($type) {
            default:
                $path = env('AWS_USER_PROFILES');
                break;
        }
        return $path;
    }
}
