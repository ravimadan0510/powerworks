<?php

namespace App\Helpers\Utility;

/*
 * This is Utility Class of the Image
 */
use App\library\aws\S3;

class ImageHelper
{
    
    public function __construct()
    {
        $this->s3 = new S3(env('AWS_ACCESS_KEY_ID'), env('AWS_SECRET_ACCESS_KEY'));
    }
    
    public function upload($image, $type, $thumb = 0)
    {
       
        $filename='';
        $thumnail_name='';
        if ($image->isValid()) {
            $extension = $image->getClientOriginalExtension();
               
            $filename = rand(1, 9999).time().'.'.$extension;
               
            $path=$this->getPath($type);
            $temp_path =  $image->getRealPath();
                
            $this->s3->putObjectFile($image->getRealPath(), env('AWS_BUCKET_NAME'), $path.$filename);
               
            if ($thumb==1) {
            }
        }
        return array('image'=>$filename);
    }
    private function getPath($type)
    {
        switch ($type) {
            default:
                $path   =   $_ENV['ECOSYSTEM_IMAGE_PATH'];
                break;
        }
        return $path;
    }
}
