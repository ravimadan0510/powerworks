<?php
namespace App\Helper\Utility;

class FileRepositoryLocal
{
    
    public function __construct()
    {
    }
    
    public function upload($image, $type, $name = '', $thumb = 0, $id = '')
    {
        $filename='';
        $thumnail_name='';
        if (is_object($image) && $image->isValid()) {
            $extension = $image->getClientOriginalExtension();
            if ($name=='') {
                $filename = time().rand(111, 999).'.'.$extension;
            } else {
                $filename = $this->getFilename($name);
            }
            $path=$this->getPath($type);
            $image->move($path, $filename);

            if ($thumb==1) {
                $f = explode(".", $filename);
                $thumnail_name=$f[0]."_thumb.png";
                exec('ffmpeg -i '.$image->getRealPath().' -ss 3 -f image2 -vframes 1 '.  $path.$thumnail_name);
            }
        }
        return DIRECTORY_SEPARATOR.$path.DIRECTORY_SEPARATOR.$filename;
    }
    
    private function getPath($type, $id = "")
    {
        switch ($type) {
            default:
                $path = env('AWS_USER_PROFILES');
                break;
        }
        return $path;
    }
    
    private function getFilename($filename)
    {
        $f = explode("/", $filename);
        $rev_arr= array_reverse($f);
        return $rev_arr[0];
    }
}
