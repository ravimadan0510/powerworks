@extends('layouts.web')

@section('content')
<div class="container">
    <!-- Breadcomb area Start-->
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcomb-list">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="breadcomb-wp">
                            <div class="breadcomb-icon">
                                <i class="notika-icon notika-form"></i>
                            </div>
                            <div class="breadcomb-ctn">
                                <h2>Dashboard</h2>
                                <p>Welcome to Dashboard.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcomb area End-->
    <div class="row mg-t-30">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="blog-inner-list notika-shadow tb-res-ds-n dk-res-ds">
                        <div class="clearfix"></div>
                        <div class="blog-ctn">
                            <div class="blog-hd-sw">
                                <h2>Add Customer</h2>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="dialog-pro dialog">
                            <a href="{{ url('/customer/add') }}" class="btn btn-primary btn-sm waves-effect">Add</a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="blog-inner-list notika-shadow tb-res-ds-n dk-res-ds">
                        <div class="blog-ctn">
                            <div class="blog-hd-sw">
                                <h2>List Customer</h2>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="dialog-pro dialog">
                            <a href="{{ url('/customer/list') }}" class="btn btn-primary btn-sm waves-effect">View All</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
