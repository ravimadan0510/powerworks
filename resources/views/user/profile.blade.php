@extends('layouts.web')

@section('content')
<div class="form-element-area">
    <div class="container">
        <!-- Breadcomb area Start-->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="breadcomb-list">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="breadcomb-wp">
                                <div class="breadcomb-icon">
                                    <i class="notika-icon notika-form"></i>
                                </div>
                                <div class="breadcomb-ctn">
                                    <h2>Profile Dashboard</h2>
                                    <p>{{ $user['first_name']." ".$user['last_name'] }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Breadcomb area End-->
        <div class="row mg-t-30">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-element-list">
                    <div class="basic-tb-hd">
                        <h2>Profile Details</h2>
                        @if(Session::has('message'))
                         <div class="alert-list">
                            <div class="alert alert-success {{ Session::get('alert-class', 'alert-info') }}" >
                                <p>{{ Session::get('message') }}</p>
                            </div>
                        </div> 
                       @endif
                    </div>
                    {!! Form::open(array('url' => '','enctype'=>'multipart/form-data','id'=>'save_form_submit')) !!}
                    {!! Form::hidden('user_id',$user['id'], []) !!}  
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group ic-cmp-int">
                                <div class="form-ic-cmp">
                                    <i class="notika-icon notika-support"></i>
                                </div>
                                <div class="nk-int-st">
                                    {!! Form::text('first_name',$user['first_name'], ['class' => 'form-control','placeholder' => 'First Name']) !!}  
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group ic-cmp-int">
                                <div class="form-ic-cmp">
                                    <i class="notika-icon notika-support"></i>
                                </div>
                                <div class="nk-int-st">
                                    {!! Form::text('last_name',$user['last_name'], ['class' => 'form-control','placeholder' => 'Last Name']) !!}  
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group ic-cmp-int">
                                <div class="form-ic-cmp">
                                    <i class="notika-icon notika-house"></i>
                                </div>
                                <div class="nk-int-st">
                                    {!! Form::text('address_line_1',$user['address_line_1'], ['class' => 'form-control','placeholder' => 'Street Address']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group ic-cmp-int">
                                <div class="form-ic-cmp">
                                    <i class="notika-icon notika-house"></i>
                                </div>
                                <div class="nk-int-st">
                                    {!! Form::text('address_line_2',$user['address_line_2'], ['class' => 'form-control','placeholder' => 'Apartment']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group ic-cmp-int">
                                <div class="form-ic-cmp">
                                    <i class="notika-icon notika-house"></i>
                                </div>
                                <div class="nk-int-st">
                                    {!! Form::text('city',$user['city'], ['class' => 'form-control','placeholder' => 'City']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group ic-cmp-int bootstrap-select">
                                <div class="form-ic-cmp">
                                    <i class="notika-icon notika-house"></i>
                                </div>
                                <div class="nk-int-st">
                                    {!! Form::text('state', $user['state'], ['class' => 'form-control','placeholder' => 'State']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group ic-cmp-int">
                                <div class="form-ic-cmp">
                                    <i class="notika-icon notika-next"></i>
                                </div>
                                <div class="nk-int-st">
                                    {!! Form::text('zipcode',$user['zipcode'], ['class' => 'form-control','placeholder' => 'Zip Code']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group nk-datapk-ctm ic-cmp-int" id="data_1">
                                <div class="form-ic-cmp">
                                    <i class="notika-icon notika-calendar"></i>
                                </div>
                                <div class="input-group date nk-int-st">
                                    <span class="input-group-addon"></span>
                                    {!! Form::text('dob',$user['dob'], ['class' => 'form-control datepicker','placeholder' => 'Date of Birth']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group ic-cmp-int">
                                <div class="form-ic-cmp">
                                    <i class="notika-icon notika-credit-card"></i>
                                </div>
                                <div class="nk-int-st">
                                    {!! Form::text('ssn',$user['ssn'], ['class' => 'form-control','placeholder' => 'SSN']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-example-int mg-t-15">
                        <div class="fm-checkbox">
                            <label>{!! Form::checkbox('is_term_accepted',$user['is_term_accepted'], ['class' => 'i-checks']) !!}
                            <i></i> Don't forget to check me out</label>
                        </div>
                    </div>
                    <div class="form-example-int mg-t-15">
                        <button class="btn btn-success notika-btn-success">Update</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script> 
var config = { 
                "updateProfile" : "{{ url('/user/post-update-profile') }}"
            };
</script>
<script src="{{asset('js/user/signup_profile.js')}}"></script>
@endpush    
