<!-- Start Header Top Area -->
<div class="header-top-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="logo-area">
                    <!--<a href="/home"><img src="{{ asset("/theme/green-horizontal/img/logo/logo.png") }}" alt="" /></a>-->
                </div>
            </div>
            
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="header-top-menu">
                    <ul class="nav navbar-nav notika-top-nav">
                        <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><span><i class="notika-icon notika-menus"></i></span></a>
                            <div role="menu" class="dropdown-menu message-dd chat-dd animated zoomIn">
                                <div class="hd-mg-tt">
                                    <h2>Powerworks</h2>
                                </div>
                                <div class="hd-message-info">
                                    <a href="{{ url('user/update-profile') }}">
                                        <div class="hd-message-sn">
                                            <div class="hd-mg-ctn">
                                                <h3>My Profile</h3>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <div class="hd-message-sn">
                                            <div class="hd-mg-ctn">
                                                <h3>Logout</h3>
                                            </div>
                                        </div>
                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Header Top Area -->
<!-- Mobile Menu start -->
<div class="mobile-menu-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="mobile-menu">
                    <nav id="dropdown">
                        <ul class="mobile-menu-nav">
                            <li><a data-toggle="collapse" data-target="#Charts" href="#">My Overview</a>
                                <ul class="collapse dropdown-header-top">
                                    <li><a href="{{ url('/home') }}">Dashboard</a></li>
                                    <li><a href="{{ url('/customer/list') }}">List Customer</a></li>
                                    <li><a href="{{ url('/customer/add') }}">Add Customer</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Mobile Menu end -->
<!-- Main Menu area start-->
<div class="main-menu-area mg-tb-40">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro">
                    <li class="active"><a data-toggle="tab" href="#MyOverView"><i class="notika-icon notika-house"></i> My OverView</a>
                    </li>
                </ul>
                <div class="tab-content custom-menu-content">
                    <div id="MyOverView" class="tab-pane in active notika-tab-menu-bg animated flipInX">
                        <ul class="notika-main-menu-dropdown">
                            <li><a href="{{ url('/home') }}">Dashboard</a></li>
                            <li><a href="{{ url('/customer/list') }}">List Customer</a></li>
                            <li><a href="{{ url('/customer/add') }}">Add Customer</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main Menu area End-->