<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                @if(!empty(Auth::user()->image_url))
                    <img id='profile-image' src="{{ env('AWS_URL').Auth::user()->image_url }}" class="img-circle" alt="User Image">
                @else
                <img id='profile-image' src="{{ URL::asset('/bower_components/admin-lte/dist/img/avatar.png') }}" class="img-circle" alt="User Image">
                @endif
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</p>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Quoting System Management</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="treeview {{ (Request::is('user/list') || Request::is('user/create') || Request::is('user/notify') ? 'active' : '') }}">
                <a href="#"><i class="fa fa-user"></i><span>Manage Users</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{ (Request::is('user/list') ? 'active' : '') }}"><a href="{{URL::to('user/list')}}">Users List</a></li>
                    <li class="{{ (Request::is('user/create') ? 'active' : '') }}"><a href="{{URL::to('user/create')}}">Add New</a></li>
                </ul>
            </li>
            <li class="treeview {{ (Request::is('contact/list') || Request::is('contact/add') ? 'active' : '') }}">
                <a href="#"><i class="fa fa-users"></i><span>Manage Contacts</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{ (Request::is('contact/list') ? 'active' : '') }}"><a href="{{URL::to('contact/list')}}">Contact List</a></li>
                    <li class="{{ (Request::is('contact/add') ? 'active' : '') }}"><a href="{{URL::to('contact/add')}}">Add New</a></li>
                </ul>
            </li>
            <li class="treeview {{ (Request::is('sku/list') || Request::is('sku/add') ? 'active' : '') }}">
                <a href="#"><i class="fa fa-anchor" aria-hidden="true"></i><span>Manage Product Sku</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{ (Request::is('sku/list') ? 'active' : '') }}"><a href="{{URL::to('sku/list')}}">Product Sku List</a></li>
                    <li class="{{ (Request::is('sku/add') ? 'active' : '') }}"><a href="{{URL::to('sku/add')}}">Add New</a></li>
                </ul>
            </li>
            <li class="treeview {{ (Request::is('product/list') || Request::is('product/view') ? 'active' : '') }}">
                <a href="#"><i class="fa fa-tasks" aria-hidden="true"></i><span>Manage Product</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li class="{{ (Request::is('product/list') ? 'active' : '') }}"><a href="{{URL::to('product/list')}}">Product List</a></li>
                </ul>
            </li>
            
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>