<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ config('app.name', 'Powerworks') }}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset("/theme/green-horizontal/img/favicon.ico") }}">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/bootstrap.min.css") }}">
    <!-- font awesome CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/font-awesome.min.css") }}">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/owl.carousel.css") }}">
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/owl.theme.css") }}">
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/owl.transitions.css") }}">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/animate.css") }}">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/normalize.css") }}">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/scrollbar/jquery.mCustomScrollbar.min.css") }}">
    <!-- wave CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/wave/waves.min.css") }}">
    <!-- Notika icon CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/notika-custom-icon.css") }}">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/main.css") }}">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/style.css") }}">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/responsive.css") }}">
    <!-- modernizr JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/vendor/modernizr-2.8.3.min.js") }}"></script>
    @stack('style')
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- Login Register area Start-->
    <div class="login-content">
        <!-- Login -->
        @yield('content')
    </div>
    <!-- Login Register area End-->
    <!-- jquery
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/vendor/jquery-1.12.4.min.js") }}"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/bootstrap.min.js") }}"></script>
    <!-- wow JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/wow.min.js") }}"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/jquery-price-slider.js") }}"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/owl.carousel.min.js") }}"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/jquery.scrollUp.min.js") }}"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/meanmenu/jquery.meanmenu.js") }}"></script>
    <!-- counterup JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/counterup/jquery.counterup.min.js") }}"></script>
    <script src="{{ asset("/theme/green-horizontal/js/counterup/waypoints.min.js") }}"></script>
    <script src="{{ asset("/theme/green-horizontal/js/counterup/counterup-active.js") }}"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/scrollbar/jquery.mCustomScrollbar.concat.min.js") }}"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/sparkline/jquery.sparkline.min.js") }}"></script>
    <script src="{{ asset("/theme/green-horizontal/js/sparkline/sparkline-active.js") }}"></script>
    <!-- flot JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/flot/jquery.flot.js") }}"></script>
    <script src="{{ asset("/theme/green-horizontal/js/flot/jquery.flot.resize.js") }}"></script>
    <script src="{{ asset("/theme/green-horizontal/js/flot/flot-active.js") }}"></script>
    <!-- knob JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/knob/jquery.knob.js") }}"></script>
    <script src="{{ asset("/theme/green-horizontal/js/knob/jquery.appear.js") }}"></script>
    <script src="{{ asset("/theme/green-horizontal/js/knob/knob-active.js") }}"></script>
    <!--  Chat JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/chat/jquery.chat.js") }}"></script>
    <!--  wave JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/wave/waves.min.js") }}"></script>
    <script src="{{ asset("/theme/green-horizontal/js/wave/wave-active.js") }}"></script>
    <!-- icheck JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/icheck/icheck.min.js") }}"></script>
    <script src="{{ asset("/theme/green-horizontal/js/icheck/icheck-active.js") }}"></script>
    <!--  todo JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/todo/jquery.todo.js") }}"></script>
    <!-- Login JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/login/login-action.js") }}"></script>
    <!-- plugins JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/plugins.js") }}"></script>
    <!-- main JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/main.js") }}"></script>
    
</body>

</html>