<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ config('app.name', 'Powerworks') }}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/bootstrap.min.css") }}">
    <!-- font awesome CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/font-awesome.min.css") }}">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/owl.carousel.css") }}">
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/owl.theme.css") }}">
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/owl.transitions.css") }}">
    <!-- meanmenu CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/meanmenu/meanmenu.min.css") }}">
    <!-- summernote CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/summernote/summernote.css") }}">
    <!-- Range Slider CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/themesaller-forms.css") }}">
    <!-- Notika icon CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/notika-custom-icon.css") }}">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/scrollbar/jquery.mCustomScrollbar.min.css") }}">
    <!-- Data Table JS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/jquery.dataTables.min.css") }}">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/animate.css") }}">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/normalize.css") }}">
    <!-- bootstrap select CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/bootstrap-select/bootstrap-select.css") }}">
    <!-- datapicker CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/datapicker/datepicker3.css") }}">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/chosen/chosen.css") }}">
    <!-- wave CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/wave/waves.min.css") }}">
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/wave/button.css") }}">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/main.css") }}">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/style.css") }}">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset("/theme/green-horizontal/css/responsive.css") }}">
    <!-- modernizr JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/vendor/modernizr-2.8.3.min.js") }}"></script>
    
    @stack('style')
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- Start Header -->
    @include('layouts.section.header')
    <!-- End Header -->
    <!-- Content start-->
        @if (session('success-message'))
            @php
                $message = session('success-message');
                $class = '';
                \App\Helper\Utility\UtilityHelper::forgetSession('success-message');
            @endphp
        @else
            @php
                $message = '';
                $class = 'hide';
            @endphp
        @endIf
        <div class="container">
            <div class="row">
                <div class="alert alert-success {{ $class }} alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <p class="alert-content">{{ $message }}</p>
                </div>
                <div class="alert alert-danger hide alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <p class="alert-content"></p>
                </div>
            </div>
        </div>
        @yield('content')
    <!-- Content End-->
    <!-- Start Footer area-->
    @include('layouts.section.footer')
    <!-- End Footer area-->
    <!-- jquery
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/vendor/jquery-1.12.4.min.js") }}"></script>
    <!-- bootstrap JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/bootstrap.min.js") }}"></script>
    <!-- wow JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/wow.min.js") }}"></script>
    <!-- price-slider JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/jquery-price-slider.js") }}"></script>
    <!-- owl.carousel JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/owl.carousel.min.js") }}"></script>
    <!-- scrollUp JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/jquery.scrollUp.min.js") }}"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/meanmenu/jquery.meanmenu.js") }}"></script>
    <!-- bootstrap select JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/bootstrap-select/bootstrap-select.js") }}"></script>
    <!-- counterup JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/counterup/jquery.counterup.min.js") }}"></script>
    <script src="{{ asset("/theme/green-horizontal/js/counterup/waypoints.min.js") }}"></script>
    <script src="{{ asset("/theme/green-horizontal/js/counterup/counterup-active.js") }}"></script>
    <!-- sparkline JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/sparkline/jquery.sparkline.min.js") }}"></script>
    <script src="{{ asset("/theme/green-horizontal/js/sparkline/sparkline-active.js") }}"></script>
    <!-- knob JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/knob/jquery.knob.js") }}"></script>
    <script src="{{ asset("/theme/green-horizontal/js/knob/jquery.appear.js") }}"></script>
    <script src="{{ asset("/theme/green-horizontal/js/knob/knob-active.js") }}"></script>
    <!-- Input Mask JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/jasny-bootstrap.min.js") }}"></script>
    <!-- icheck JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/icheck/icheck.min.js") }}"></script>
    <script src="{{ asset("/theme/green-horizontal/js/icheck/icheck-active.js") }}"></script>
    <!-- datapicker JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/datapicker/bootstrap-datepicker.js") }}"></script>
    <script src="{{ asset("/theme/green-horizontal/js/datapicker/datepicker-active.js") }}"></script>
    <!-- dropzone JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/dropzone/dropzone.js") }}"></script>
    <!--  chosen JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/chosen/chosen.jquery.js") }}"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/scrollbar/jquery.mCustomScrollbar.concat.min.js") }}"></script>
    <!-- flot JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/flot/jquery.flot.js") }}"></script>
    <script src="{{ asset("/theme/green-horizontal/js/flot/jquery.flot.resize.js") }}"></script>
    <script src="{{ asset("/theme/green-horizontal/js/flot/jquery.flot.time.js") }}"></script>
    <script src="{{ asset("/theme/green-horizontal/js/flot/jquery.flot.tooltip.min.js") }}"></script>
    <script src="{{ asset("/theme/green-horizontal/js/flot/analtic-flot-active.js") }}"></script>
	<!--  wave JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/wave/waves.min.js") }}"></script>
    <script src="{{ asset("/theme/green-horizontal/js/wave/wave-active.js") }}"></script>
    <!-- plugins JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/plugins.js") }}"></script>
    <!-- Data Table JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/data-table/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("/theme/green-horizontal/js/data-table/data-table-act.js") }}"></script>
    <!-- main JS
		============================================ -->
    <script src="{{ asset("/theme/green-horizontal/js/main.js") }}"></script>
    <script src="{{ asset ("/js/jquery.validate.min.js") }}" type="text/javascript"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd'
        });
        
        $(".alert").fadeTo(5000, 500).slideUp(500, function(){
            $(".alert").slideUp(500);
        });
    </script>
    @stack('scripts')
</body>

</html>
