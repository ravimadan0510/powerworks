@extends('layouts.web')

@section('content')
<div class="container">
    <!-- Breadcomb area Start-->
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="breadcomb-list">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="breadcomb-wp">
                            <div class="breadcomb-icon">
                                <i class="notika-icon notika-form"></i>
                            </div>
                            <div class="breadcomb-ctn">
                                <h2>List Customer</h2>
                                <p>List and search customer based on filters.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcomb area End-->
     <!-- Form Element area Start-->
    <div class="form-element-area">
        {!! Form::open(array('url' => '','enctype'=>'multipart/form-data', 'id'=> 'customer-search-form')) !!}
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-element-list mg-t-30">
                    <div class="cmp-tb-hd">
                        <h2>Search by Name</h2>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group ic-cmp-int">
                                <div class="form-ic-cmp">
                                    <i class="notika-icon notika-search"></i>
                                </div>
                                <div class="nk-int-st">
                                    {!! Form::text('name',old('name'), ['class' => 'form-control','placeholder' => 'Name', 'id' => 'name']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-element-list mg-t-30">
                    <div class="cmp-tb-hd">
                        <h2>Search by Age, Gender or Clothing Size</h2>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="cmp-tb-hd">
                                <h2>Age</h2>
                            </div>
                            <div class="nk-int-st">
                                <input type="text" name="age" id="age" class="form-control" Placeholder="Age">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="cmp-tb-hd">
                                <h2>Gender</h2>
                            </div>
                            <div class="toggle-select-act fm-cmp-mg mg-t-20">
                                {!! Form::select('gender', $genderList, old('gender'), ['class' => 'selectpicker', 'id' => 'gender', 'placeholder' => 'Gender', 'data-live-search' => true]) !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="cmp-tb-hd">
                                <h2>Clothing Size</h2>
                            </div>
                            <div class="toggle-select-act fm-cmp-mg mg-t-20">
                                {!! Form::select('clothing_size', $clothingSize, old('clothing_size'), ['class' => 'selectpicker', 'id' => 'clothing_size', 'placeholder' => 'Clothing Size', 'data-live-search' => true]) !!}
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-element-list mg-t-30">
                                <div class="cmp-tb-hd">
                                    <button class="btn btn-success notika-btn-success waves-effect">Search</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- Form Element area End-->
    <div class="data-table-area">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="data-table-list">
                    <div class="basic-tb-hd">
                        <h2>Search Results</h2>
                    </div>
                    <div class="table-responsive">
                        <table id="customerData" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Gender</th>
                                    <th>Age</th>
                                    <th>Clothing Size</th>
                                    <th>Front Image</th>
                                    <th>Side Image #1</th>
                                    <th>Side Image #2</th>
                                    <th>Created At</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script> 
    
    var config = { 
                   "data" : "{{ url('/customer/list-data') }}",
                   "imageUrl" : "{{ url('/uploads').DIRECTORY_SEPARATOR }}"
               };
               
    table = $('#customerData').DataTable({
        processing: true,
        serverSide: true,
        searching: false,
        ajax: {
            url: config.data,
            data: function (d) {
                d.name = $('#name').val();
                d.age = $('#age').val();
                d.clothing_size = $('#clothing_size').val();
                d.gender = $('#gender').val();
            }
        },
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'gender', name: 'gender' },
            { data: 'age', name: 'age' },
            { data: 'clothing_size', name: 'clothing_size' },
            { 
                data: 'front_image_url', 
                name: 'front_image_url',
                render: function (data, type, row) {
                    if(row.front_image_url == "" || row.front_image_url == null) {
                        return '<div style="width : 150px; height:150px">N/A</div>';
                    }
                    return '<img src="'+config.imageUrl+row.front_image_url+'" style="width : 150px; height:150px" />';
                }
            },
            { 
                data: 'side_image_url_1', 
                name: 'side_image_url_1',
                render: function (data, type, row) {
                    if(row.side_image_url_1 == "" || row.side_image_url_1 == null) {
                        return '<div style="width : 150px; height:150px">N/A</div>';
                    }
                    return '<img src="'+config.imageUrl+row.side_image_url_1+'" style="width : 150px; height:150px" />';
                }
            },
            { 
                data: 'side_image_url_2', 
                name: 'side_image_url_2',
                render: function (data, type, row) {
                    if(row.side_image_url_2 == "" || row.side_image_url_2 == null) {
                        return '<div style="width : 150px; height:150px">N/A</div>';
                    }
                    return '<img src="'+config.imageUrl+row.side_image_url_2+'" style="width : 150px; height:150px" />';
                }
            },
            { data: 'created_at', name: 'created_at' }
        ]
    });
    
    $('#customer-search-form').on('submit', function(e) {
        table.draw();
        e.preventDefault();
    });
</script>
@endpush  
