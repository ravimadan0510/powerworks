@extends('layouts.admin_login')

@section('content')
<div class="nk-block toggled" id="l-login">
    <div class="nk-form">
        <p class="text-left">{{ __('Sign In') }}</p>
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="input-group">
                <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-support"></i></span>
                <div class="nk-int-st">
                    <input id="email" type="text" name="email" value="{{ old('email') }}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" autofocus>
                </div>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="input-group mg-t-15">
                <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-edit"></i></span>
                <div class="nk-int-st">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password">
                </div>
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="fm-checkbox">
                <label><input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} class="i-checks"> <i></i>{{ __('Remember Me') }}</label>
            </div>
            <button type="submit" class="btn btn-login btn-success btn-float">
                <i class="notika-icon notika-right-arrow right-arrow-ant"></i>
            </button>
        </form>
    </div>

    <div class="nk-navigation nk-lg-ic">
        <a href="{{ route('register') }}" data-ma-block="#l-register"><i class="notika-icon notika-plus-symbol"></i> <span>Register</span></a>
        <a href="{{ route('password.request') }}" data-ma-block="#l-forget-password"><i>?</i> <span>Forgot Password</span></a>
    </div>
</div>
@endsection
