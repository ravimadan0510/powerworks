@extends('layouts.admin_login')

@section('content')
<!-- Forgot Password -->
<div class="nk-block toggled" id="l-forget-password">
    <div class="nk-form">
        <p class="text-left">{{ __('Reset Password') }}</p>
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <form method="POST" action="{{ route('password.email') }}">
            @csrf
            <div class="input-group">
                <span class="input-group-addon nk-ic-st-pro"><i class="notika-icon notika-mail"></i></span>
                <div class="nk-int-st">
                    <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" class="form-control" placeholder="Email Address">
                </div>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <button type="submit" class="btn btn-login btn-success btn-float">
                <i class="notika-icon notika-right-arrow right-arrow-ant"></i>
            </button>
        </form>
    </div>

    <div class="nk-navigation nk-lg-ic rg-ic-stl">
        <a href="{{ route('login') }}" data-ma-block="#l-login"><i class="notika-icon notika-right-arrow"></i> <span>Sign in</span></a>
        <a href="{{ route('register') }}" data-ma-block="#l-register"><i class="notika-icon notika-plus-symbol"></i> <span>Register</span></a>
    </div>
</div>
@endsection
